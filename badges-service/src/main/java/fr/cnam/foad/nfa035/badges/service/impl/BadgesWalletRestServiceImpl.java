package fr.cnam.foad.nfa035.badges.service.impl;

import fr.cnam.foad.nfa035.badges.service.BadgesWalletRestService;
import fr.cnam.foad.nfa035.badges.wallet.dao.impl.json.JSONBadgeWalletDAO;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

import java.io.IOException;
import java.util.Set;

/**
 * classe d'implémentation du service rest pour l'application
 */
public class BadgesWalletRestServiceImpl implements BadgesWalletRestService {
    
    private JSONBadgeWalletDAO jSONBadgeWalletDAO;
    
    public void deleteBadge() {
        // TODO: 21/12/2022  
    }

    /**
     * Lecture du Wallet => R
     *
     * @return ResponseEntity la réponse REST toujours
     */
    @Operation(summary = "Récupère le métadonnées du Wallet",
            description = "Récupère le métadonnées du portefeuille, c'est à dire l'index des badges qui s'y trouve") //documente le service
    @Tag(name = "getMetadata") //documente le service
    @GetMapping("/metas") //définit le comportement Restful de la méthode
    public
    ResponseEntity<Set<DigitalBadge>> getMetadata() throws IOException {
        return ResponseEntity.ok().body(jSONBadgeWalletDAO.getWalletMetadata());
    }
    
    public void putBadges() {
        // TODO: 21/12/2022
    }
    
    public void readBadges() {
        // TODO: 21/12/2022  
    }
    
}
