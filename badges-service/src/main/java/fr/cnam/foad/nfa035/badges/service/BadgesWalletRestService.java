package fr.cnam.foad.nfa035.badges.service;

import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

import java.io.IOException;
import java.util.Set;

/**
 * classe interface pour le service rest de l'application
 */
public interface BadgesWalletRestService {

    public void deleteBadge();



    /**
     * Lecture du Wallet => R
     *
     * @return ResponseEntity la réponse REST toujours
     */
    @Operation(summary = "Récupère le métadonnées du Wallet",
            description = "Récupère le métadonnées du portefeuille, c'est à dire l'index des badges qui s'y trouve")
    //documente le service
    @Tag(name = "getMetadata") //documente le service
    @GetMapping("/metas")
    //définit le comportement Restful de la méthode
    ResponseEntity<Set<DigitalBadge>> getMetadata() throws IOException;

    public void putBadges();

    public void readBadges();

}
