# Applications WEB, MVC, Services Rest
 ++ Pré-requis au Développement d'une application Web
 

## Contexte
* Au programme de ce cours: Génération et Parsage JSON 

Notre startup a fait le buzz auprès de ses investisseurs grâce au prototype développé avec Swing. 
Le hic, c'est qu'à présent, pour avancer, nous devons accepter l'introduction d'un nouvel investisseur prenant le rôle de CTO, car ayant la confiance du BOard en matière d'industrialisation logicielle.
Ce nouveau CTO ainsi proclamé, un certain nombre de pré-requis nous sont alors imposés.

# Applications WEB, MVC, Services Rest
 ++ Pré-requis au Développement d'une application Web
 

## Contexte
* Au programme de ce cours: Services Rest, scaffolding Web

Comme à son habitude, notre meilleur client revient nous voir avec un fort enthousiasme.
Il a eu vent des prjets de refonte JSON, et aussi de l'éventuelle création de services Rest. IL n'a vraiment pas tout compris, mais il commence à répendre la nouvelle que notre architecture est déjà 100% Restful
EN plus de ça, il nous demande de prototyper rapidement une interface Web s'appuyant sur nos services...mais donc, il faut les développer de toute urgence !

## Objectifs
* Mises en application:
- [x] (Exercice 1) Normalisation JSON en pré-requis au passage en application WEB
- [x] (Exercice 2) BONUS de méthodes utilitaires, exceptions pour détection de fraude, Utilisation d'un Tableau associatif
- [x] (Exercice 3) Homogénéisation Maven + Spring
- [x] **(Exercice 4) Services Rest + Scaffolding simple (BONUS)**

----

Comme vous l'avez probablement lu plus haut, nous allons donc tenter d'arriver rapidement, donc simplement à un premeir résultat probant de service Rest, et ensuite nous tenterons d'échafauder une interface Web.

### Premier Service Rest

 - [ ] Commençons par créer un nouveau sous-module java, nommé **badges-service**
 - [ ] Ces dépendances suffisent:
   ```xml
          <dependency>
              <groupId>fr.cnam.foad.nfa035.badges</groupId>
              <artifactId>badges-wallet</artifactId>
              <version>${project.version}</version>
              <scope>compile</scope>
          </dependency>

          <dependency>
              <groupId>org.springframework.boot</groupId>
              <artifactId>spring-boot-starter-web</artifactId>
          </dependency>

          <!-- https://mvnrepository.com/artifact/io.swagger.core.v3/swagger-core -->
          <dependency>
              <groupId>io.swagger.core.v3</groupId>
              <artifactId>swagger-core</artifactId>
              <version>2.1.10</version>
          </dependency>

          <!-- https://mvnrepository.com/artifact/org.springdoc/springdoc-openapi-ui -->
          <dependency>
              <groupId>org.springdoc</groupId>
              <artifactId>springdoc-openapi-ui</artifactId>
              <version>1.5.10</version>
          </dependency>
     ```
 - [ ] A présent nous allons intégrer une nouvelle interface correspon dant au service Rest à implémenter. DOnc ebn suivant le diagramme classe ci-dessous, coder l'ossature de classe concrête et son interface.
   ```plantuml
   @startuml
   title __IMPL's Class Diagram__\n
   
   namespace fr.cnam.foad.nfa035.badges {
   namespace service {
   namespace impl {
   class fr.cnam.foad.nfa035.badges.service.impl.BadgesWalletRestServiceImpl {
   + deleteBadge()
     + getMetadata()
     + putBadge()
     + readBadge()
     }
     }
     }
     }
   fr.cnam.foad.nfa035.badges.service.impl.BadgesWalletRestServiceImpl .up.|> fr.cnam.foad.nfa035.badges.service.BadgesWalletRestService
   fr.cnam.foad.nfa035.badges.service.impl.BadgesWalletRestServiceImpl o-- fr.cnam.foad.nfa035.badges.wallet.dao.impl.json.JSONBadgeWalletDAO : jsonBadgeDao
   @enduml
   ```
 - [ ] Suite à ce premier codage, nous allons affiner méthode par méthode. IL faut que cela compile toutefois. A présent voici le prototype de l'opération **getMetadata**
   ```java
    /**
     * Lecture du Wallet => R
     *
     * @return ResponseEntity la réponse REST toujours
     */
    @Operation(summary = "Récupère le métadonnées du Wallet",
            description = "Récupère le métadonnées du portefeuille, c'est à dire l'index des badges qui s'y trouve"
    )
    @Tag(name = "getMetadata")
    @GetMapping("/metas")
    ResponseEntity<Set<DigitalBadge>> getMetadata();
   ```
 - Notez l'utilisation des annotations 
   - **@GetMapping** issue de Spring et qui permet de définir le comportement Restful de la méthode, c'est-à-dire qu'il s'agit d'un appel REST de type GET (https://docs.spring.io/spring-framework/docs/current/reference/html/web.html#mvc-ann-requestmapping-uri-templates)
   - **@Tag** et **@Operation** issued de Swagger, librairie que nous avons intégrée dans le pom et qui va nous permettre de documenter ce service Rest à la façon d'une API de service RestFul 
     - Vous devez vous documenter également sur celles-ci, ici: https://swagger.io/specification/ et là https://docs.swagger.io/swagger-core/v2.0.0-RC3/apidocs/io/swagger/v3/oas/annotations/package-summary.html
 - [ ] Enfin Dans votre implémentation, vous devez respecter le pattern **Builder**, ou devrais-je dire, que c'est plus facile comme ça, qui consite à utiliser en chaîne les méthodes statique de la classe de l'objet de retour, **ResponseEntity**. Cela donne:
   ```java
     return ResponseEntity
              .ok()
              .body(jsonBadgeDao.getWalletMetadata())
   ```
 - [ ] Vous l'aurez compris, il faudra également veiller à
   - [ ] Injecter le DAO
   - [ ] Annoter l'implémentation avec **@RestController**
 - [ ] Ajouter une classe d'application spring boot dans le package de base du module, **service**:
 
  ```java
       @OpenAPIDefinition(
           info = @Info(
                   title = "Service d'accès au Portefeuille de Badge",
                   version = "1.0.0-SNAPSHOT",
                   description = "API permettant la manipulation d'entité de type DigitalBadges au sein de notre portefeuille au format JSON (Wallet). " +
                           "<br/>Il s'agit simplement, en termes d'opérations, de réponde aux exigences CRUD (Create, Read, Update, et Delete), ",
                   license = @License(name = "Tous droits réservés", url = "https://lecnam.net/"),
                   contact = @Contact(name = "<Votre Nom>", email = "<votre mail>>")
           )
      )
      @SpringBootApplication
      public class BadgesServiceApplication {
         public static void main(String[] args) {
            SpringApplication.run(BadgesServiceApplication.class, args);
         }
      
      }
   ```
- [ ] *Note: Il m'a fallu rajouter une dépendance pour slf4j sinon compilation impossible*
```xml
        <!-- https://mvnrepository.com/artifact/org.slf4j/slf4j-api -->
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-api</artifactId>
            <version>2.0.6</version>
        </dependency>
```

   
## Tests via la documentation

 - [ ] C'est donc le moment de lancer l'application de service
   ![preuve1][preuve1]
 - [ ] Observer les logs pour connaître le port par défaut
   ![preuve2][preuve2]
 - [ ] Ce que génère swagger, c'est juste ce fichier json qui représente le contrat de nos services, accessible à cette adresse: http://localhost:8080/v3/api-docs
   ![api](screenshots/api.png)
 - [ ] Par contre swagger-ui est accessible à cette adresse: http://localhost:8080/swagger-ui/index.html et plus précisément pour le seul service que nous avons réellement implémenter pour le moment:
    * http://localhost:8080/swagger-ui/index.html?configUrl=/v3/api-docs/swagger-config#/getMetadata/getMetadata
    ![test-api](screenshots/test-api.png)
 - [ ] Essayez de lancer le service et apportez une preuve que vous recevez bien de cette façon le contenu de votre wallet en retour (métadonnées bien sûr)
   * Il faut savoir que cette interface graphique n'est autre qu'un client écrit en JavaScript, et qui consomme ce fichier json avant de vous restituer le contenu graphiquement de façon lisible, et en plus vous permettant de consommer les services.. C'est plutôt fort non ?
 - [ ] Pour ce service c'est vrai qu ce n'est pas bien compliqué à tester , il suffit de copier cette adresse dans le navigateur... mais pour d'autres, ça peu devenr difficile...
   - http://localhost:8080/_badges/metas
    ![test-api2](screenshots/test-api2.png)
 - [ ] Essayez aussi cette commande que le swagger vous donne, qu'est-ce que cela donne depuis votre git-bash ?
   ```
   curl -X 'GET' \
   'http://localhost:8080/_badges/metas' \
   -H 'accept: */*'
   ```
- [ ] *J'ai une erreur au lancement de BadgeServiceapplication :*
- ![error][]
- [ ] *le terminal ne me renvoie pourtant pas de message d'erreur particulier :*
```java
C:\Java\jdk-17.0.4.1-full\bin\java.exe "-javaagent:C:\Program Files\JetBrains\IntelliJ IDEA Community Edition 2022.2.1\lib\idea_rt.jar=50414:C:\Program Files\JetBrains\IntelliJ IDEA Community Edition 2022.2.1\bin" -Dfile.encoding=UTF-8 -classpath C:\Users\ThierryV\Jetbrains\JAVA\Project_01\src\NFA035\exercice16\exercice-4\badges-service\target\classes;C:\Users\ThierryV\Jetbrains\JAVA\Project_01\src\NFA035\exercice16\exercice-4\badges-wallet\target\classes;C:\Users\ThierryV\.m2\repository\org\apache\logging\log4j\log4j-core\2.14.1\log4j-core-2.14.1.jar;C:\Users\ThierryV\.m2\repository\org\apache\logging\log4j\log4j-api\2.14.1\log4j-api-2.14.1.jar;C:\Users\ThierryV\.m2\repository\commons-codec\commons-codec\1.15\commons-codec-1.15.jar;C:\Users\ThierryV\.m2\repository\commons-io\commons-io\2.11.0\commons-io-2.11.0.jar;C:\Users\ThierryV\.m2\repository\org\apache\commons\commons-lang3\3.12.0\commons-lang3-3.12.0.jar;C:\Users\ThierryV\.m2\repository\com\fasterxml\jackson\core\jackson-core\2.12.5\jackson-core-2.12.5.jar;C:\Users\ThierryV\.m2\repository\com\fasterxml\jackson\core\jackson-databind\2.12.5\jackson-databind-2.12.5.jar;C:\Users\ThierryV\.m2\repository\org\springframework\boot\spring-boot-starter-web\3.0.0\spring-boot-starter-web-3.0.0.jar;C:\Users\ThierryV\.m2\repository\org\springframework\boot\spring-boot-starter-json\3.0.0\spring-boot-starter-json-3.0.0.jar;C:\Users\ThierryV\.m2\repository\com\fasterxml\jackson\datatype\jackson-datatype-jdk8\2.14.1\jackson-datatype-jdk8-2.14.1.jar;C:\Users\ThierryV\.m2\repository\com\fasterxml\jackson\module\jackson-module-parameter-names\2.14.1\jackson-module-parameter-names-2.14.1.jar;C:\Users\ThierryV\.m2\repository\org\springframework\boot\spring-boot-starter-tomcat\3.0.0\spring-boot-starter-tomcat-3.0.0.jar;C:\Users\ThierryV\.m2\repository\org\apache\tomcat\embed\tomcat-embed-core\10.1.1\tomcat-embed-core-10.1.1.jar;C:\Users\ThierryV\.m2\repository\org\apache\tomcat\embed\tomcat-embed-el\10.1.1\tomcat-embed-el-10.1.1.jar;C:\Users\ThierryV\.m2\repository\org\apache\tomcat\embed\tomcat-embed-websocket\10.1.1\tomcat-embed-websocket-10.1.1.jar;C:\Users\ThierryV\.m2\repository\org\springframework\spring-web\6.0.2\spring-web-6.0.2.jar;C:\Users\ThierryV\.m2\repository\org\springframework\spring-beans\6.0.2\spring-beans-6.0.2.jar;C:\Users\ThierryV\.m2\repository\io\micrometer\micrometer-observation\1.10.0\micrometer-observation-1.10.0.jar;C:\Users\ThierryV\.m2\repository\io\micrometer\micrometer-commons\1.10.0\micrometer-commons-1.10.0.jar;C:\Users\ThierryV\.m2\repository\org\springframework\spring-webmvc\6.0.2\spring-webmvc-6.0.2.jar;C:\Users\ThierryV\.m2\repository\org\springframework\spring-aop\6.0.2\spring-aop-6.0.2.jar;C:\Users\ThierryV\.m2\repository\org\springframework\spring-context\6.0.2\spring-context-6.0.2.jar;C:\Users\ThierryV\.m2\repository\org\springframework\spring-expression\6.0.2\spring-expression-6.0.2.jar;C:\Users\ThierryV\.m2\repository\io\swagger\core\v3\swagger-core\2.2.6\swagger-core-2.2.6.jar;C:\Users\ThierryV\.m2\repository\jakarta\xml\bind\jakarta.xml.bind-api\2.3.2\jakarta.xml.bind-api-2.3.2.jar;C:\Users\ThierryV\.m2\repository\jakarta\activation\jakarta.activation-api\1.2.1\jakarta.activation-api-1.2.1.jar;C:\Users\ThierryV\.m2\repository\com\fasterxml\jackson\core\jackson-annotations\2.13.4\jackson-annotations-2.13.4.jar;C:\Users\ThierryV\.m2\repository\com\fasterxml\jackson\dataformat\jackson-dataformat-yaml\2.13.4\jackson-dataformat-yaml-2.13.4.jar;C:\Users\ThierryV\.m2\repository\com\fasterxml\jackson\datatype\jackson-datatype-jsr310\2.13.4\jackson-datatype-jsr310-2.13.4.jar;C:\Users\ThierryV\.m2\repository\io\swagger\core\v3\swagger-annotations\2.2.6\swagger-annotations-2.2.6.jar;C:\Users\ThierryV\.m2\repository\org\yaml\snakeyaml\1.33\snakeyaml-1.33.jar;C:\Users\ThierryV\.m2\repository\io\swagger\core\v3\swagger-models\2.2.6\swagger-models-2.2.6.jar;C:\Users\ThierryV\.m2\repository\jakarta\validation\jakarta.validation-api\2.0.2\jakarta.validation-api-2.0.2.jar;C:\Users\ThierryV\.m2\repository\org\springdoc\springdoc-openapi-ui\1.6.14\springdoc-openapi-ui-1.6.14.jar;C:\Users\ThierryV\.m2\repository\org\springdoc\springdoc-openapi-webmvc-core\1.6.14\springdoc-openapi-webmvc-core-1.6.14.jar;C:\Users\ThierryV\.m2\repository\org\springdoc\springdoc-openapi-common\1.6.14\springdoc-openapi-common-1.6.14.jar;C:\Users\ThierryV\.m2\repository\org\webjars\swagger-ui\4.15.5\swagger-ui-4.15.5.jar;C:\Users\ThierryV\.m2\repository\org\webjars\webjars-locator-core\0.52\webjars-locator-core-0.52.jar;C:\Users\ThierryV\.m2\repository\io\github\classgraph\classgraph\4.8.149\classgraph-4.8.149.jar;C:\Users\ThierryV\.m2\repository\org\slf4j\slf4j-api\2.0.6\slf4j-api-2.0.6.jar;C:\Users\ThierryV\.m2\repository\org\springframework\boot\spring-boot-starter\3.0.0\spring-boot-starter-3.0.0.jar;C:\Users\ThierryV\.m2\repository\org\springframework\boot\spring-boot\3.0.0\spring-boot-3.0.0.jar;C:\Users\ThierryV\.m2\repository\org\springframework\boot\spring-boot-autoconfigure\3.0.0\spring-boot-autoconfigure-3.0.0.jar;C:\Users\ThierryV\.m2\repository\org\springframework\boot\spring-boot-starter-logging\3.0.0\spring-boot-starter-logging-3.0.0.jar;C:\Users\ThierryV\.m2\repository\ch\qos\logback\logback-classic\1.4.5\logback-classic-1.4.5.jar;C:\Users\ThierryV\.m2\repository\ch\qos\logback\logback-core\1.4.5\logback-core-1.4.5.jar;C:\Users\ThierryV\.m2\repository\org\apache\logging\log4j\log4j-to-slf4j\2.19.0\log4j-to-slf4j-2.19.0.jar;C:\Users\ThierryV\.m2\repository\org\slf4j\jul-to-slf4j\2.0.4\jul-to-slf4j-2.0.4.jar;C:\Users\ThierryV\.m2\repository\jakarta\annotation\jakarta.annotation-api\2.1.1\jakarta.annotation-api-2.1.1.jar;C:\Users\ThierryV\.m2\repository\org\springframework\spring-core\6.0.2\spring-core-6.0.2.jar;C:\Users\ThierryV\.m2\repository\org\springframework\spring-jcl\6.0.2\spring-jcl-6.0.2.jar fr.cnam.foad.nfa035.badges.service.BadgesServiceApplication

        .   ____          _            __ _ _
        /\\ / ___'_ __ _ _(_)_ __  __ _ \ \ \ \
( ( )\___ | '_ | '_| | '_ \/ _` | \ \ \ \
        \\/  ___)| |_)| | | | | || (_| |  ) ) ) )
        '  |____| .__|_| |_|_| |_\__, | / / / /
        =========|_|==============|___/=/_/_/_/
        :: Spring Boot ::                (v3.0.0)

        2022-12-22T13:34:32.579+01:00  INFO 9556 --- [           main] f.c.f.n.b.s.BadgesServiceApplication     : Starting BadgesServiceApplication using Java 17.0.4.1 with PID 9556 (C:\Users\ThierryV\Jetbrains\JAVA\Project_01\src\NFA035\exercice16\exercice-4\badges-service\target\classes started by ThierryV in C:\Users\ThierryV\Jetbrains\JAVA\Project_01\src\NFA035\exercice16\exercice-4)
        2022-12-22T13:34:32.596+01:00  INFO 9556 --- [           main] f.c.f.n.b.s.BadgesServiceApplication     : No active profile set, falling back to 1 default profile: "default"
        2022-12-22T13:34:37.276+01:00  INFO 9556 --- [           main] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat initialized with port(s): 8080 (http)
        2022-12-22T13:34:37.315+01:00  INFO 9556 --- [           main] o.apache.catalina.core.StandardService   : Starting service [Tomcat]
        2022-12-22T13:34:37.316+01:00  INFO 9556 --- [           main] o.apache.catalina.core.StandardEngine    : Starting Servlet engine: [Apache Tomcat/10.1.1]
        2022-12-22T13:34:37.875+01:00  INFO 9556 --- [           main] o.a.c.c.C.[Tomcat].[localhost].[/]       : Initializing Spring embedded WebApplicationContext
        2022-12-22T13:34:37.882+01:00  INFO 9556 --- [           main] w.s.c.ServletWebServerApplicationContext : Root WebApplicationContext: initialization completed in 4894 ms
        2022-12-22T13:34:40.518+01:00  INFO 9556 --- [           main] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat started on port(s): 8080 (http) with context path ''
        2022-12-22T13:34:40.554+01:00  INFO 9556 --- [           main] f.c.f.n.b.s.BadgesServiceApplication     : Started BadgesServiceApplication in 9.95 seconds (process running for 11.84)
        2022-12-22T13:35:20.468+01:00  INFO 9556 --- [nio-8080-exec-1] o.a.c.c.C.[Tomcat].[localhost].[/]       : Initializing Spring DispatcherServlet 'dispatcherServlet'
        2022-12-22T13:35:20.468+01:00  INFO 9556 --- [nio-8080-exec-1] o.s.web.servlet.DispatcherServlet        : Initializing Servlet 'dispatcherServlet'
        2022-12-22T13:35:20.471+01:00  INFO 9556 --- [nio-8080-exec-1] o.s.web.servlet.DispatcherServlet        : Completed initialization in 1 ms

```

[preuve1]: screenshots/run-app.png "lancement du service Rest"
[preuve2]: screenshots/boot.png "lancement du service Rest"
[error]: screenshots/error.png "erreur lancement Application"

----

# BONUS (non noté): Scaffolding d'une appli Web en Angular avec le générateur openAPI

Angular est un framework MVC appliquant notamment le pattern Observer/Observable.
Il a la particularité de ne s'éxécuter que côté front, c'est à dire sur un navigateur, et doit donc s'appuyer sur des services Rest pour interagir avec des serveur / bases de données.
Il a été développé en JavaScript, mais permet depuis sa version 2 de s'appuyer sur TypeScript qui apporte un formalisme objet, à typage fort, s'apparentant qui plus est à Java.

 - [ ] Donc pour relever ce défi, rendez-vous [ICI](badges-frontend/README.md)
 - [ ] Vous verrez comment générer rapidement une application cliente consommatrice de ce service à l'aide de nodejs et des générateurs de code proposés par openAPI
   ![resultat](badges-frontend/img/angular.png)


- [ ] *Note: Il m'a fallu mettre à jour Node.js à  la version 18 sinon impossible à lancer :*
- ![node][]
- [ ] *L'installation d'Angular se passe bien :*
- ![angular4][]
- [ ] *L'écran d'accueil d'Anglar s'ouvre bien :*
- ![angular-run][]
- [ ] *Mais lors de l'installation du module npm openapi-generator-cli j'ai systématiquement une erreur* :
- ![angular8][]


[node]: screenshots/node.png "MAJ node"
[angular4]: screenshots/angular4.png "install Angular"
[angular-run]: screenshots/angular-run.png "accueil Angular"
[angular8]: screenshots/angular8.png "erreur module"